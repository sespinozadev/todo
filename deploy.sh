#!/bin/bash
shards install

# uncomment if you want build the binary in your machine but you'll use it in another computer take into account that this wil make the binary heavier.
#shards build --release --no-debug --link-flags="-static"
shards build --release --no-debug

mkdir -p ~/.local/bin/
cp bin/todo ~/.local/bin/todo